import re, discord
import msg, fireconn
from fireconn import Firebase
from discord.ext import commands
import fireconn
from discord.ext.commands import MissingRequiredArgument, UserInputError
from discord.ext.commands import ArgumentParsingError

class Participants(commands.Cog):
    '''
    Commands that only league participants should be able to use
    '''

    def __init__(self, bot):
        self.bot = bot



    async def _remove_player(self, teams_col):
        for team_doc in teams_col.stream():
            team_data = team_doc.to_dict()
            if ctx.author.id in team_data['members']:
                # .remove tends to raise ValueError
                try:
                    team_data['members'].remove(ctx.author.id)
                    if len(team_data) == 0:
                        team_doc.delete()
                    else:
                        team_doc.update(team_data)

                    return
                except:
                    continue


    @commands.command(name='leave')
    @commands.guild_only()
    async def leave_event(self, ctx, *, league:str):
        '''
        .leave Event name
        '''
        event_ref = Firebase.event(ctx.guild.id, league)
        if not event_ref.get().exists:
            await ctx.send(f'{league} was not found')
            return
        else:
            self._remove_player(Firebase.teams(ctx.guild.id, league))
            await ctx.send(':white_check_mark:')


    @commands.command(name='reportscore')
    @commands.guild_only()
    async def report_score(self, ctx, event:str, match_id:str, score:int):
        event_doc = Firebase.event(ctx.guild.id, event)
        e_details = event_doc.get().to_dict()

        if e_details['active'] is False:
            await ctx.send('Event is not yet active')
            return

        if score > e_details['maxscore']:
            await ctx.send(f'{score} is too high\nMax score: **{e_details["maxscore"]}**')
        elif score < 0: 
            await ctx.send('Score must be greater than or equal to 0')
        else:
            # Get the ref to the right team
            match_doc = None
            for match in Firebase.matches(ctx.guild.id, event).stream():
                if match.id == match_id: 
                    match_doc = match
                    break
            else:
                await ctx.send('Match was not found')
                return

            match_d = match_doc.to_dict()
            if match_d['done']:
                await ctx.send('Can\'t report scores on this match anymore')
            else:
                team_snapshot = Firebase.team(ctx.guild.id, event, ctx.author.id)
                match_d[team_snapshot.id] = score
                match_doc.reference.update(match_d)
                await ctx.send(':white_check_mark: Score reported')

    @report_score.error
    async def report_score_err(self, ctx, error):
        if isinstance(error, MissingRequiredArgument):
            await ctx.send('Format: `.reportscore "Event name" match_id score`')
        else:
            print(error)

    @leave_event.error
    async def leave_error_handler(self, ctx, error):
        if isinstance(error, MissingRequiredArgument):
            await ctx.send(f'Missing event name to leave from ')
        else:
            print(error)


def setup(bot):
    bot.add_cog(Participants(bot))
