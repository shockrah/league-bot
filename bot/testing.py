import fireconn
from discord.ext import commands
from discord.ext.commands import BadArgument, MissingRequiredArgument, UserInputError
from discord.ext.commands import ConversionError
import discord
from fireconn import Firebase

class BadInput(UserInputError):
    def __init__(self, message=None, **kw):
        self.msg = message
        self.kwargs = kw

    def __str__(self):
        k_str = ''
        for key in self.kwargs:
            k_str += f'{key}: {self.kwargs[key]}\n'

        return k_str




def restrict_testing_guild(func):
    test_guild = 684350632580218961
    # must pass a reference to the object as its consumed first
    async def callee_func(obj, ctx, *params):
        if ctx.guild.id != test_guild:
            return None
        else:
            print('TEST GUILD OUTPUT')
            return await func(obj, ctx, *params)

    return callee_func

class Testing(commands.Cog):
    def __init__(self, bot):
        print('Initializing Testing cog')
        self.bot = bot


    def restrict_guild(self, func):
        test_guild = 684350632580218961
        # must pass a reference to the object as its consumed first
        async def callee_func(obj, ctx, *params):
            if ctx.guild.id != test_guild:
                return None
            else:
                print('TEST GUILD OUTPUT')
                return await func(obj, ctx, *params)

        return callee_func

    @commands.command(name='raise')
    async def raise_err(self, ctx, word:str):
        if word != 'err':
            raise BadInput('input is an err', word=word)

        else:
            await ctx.send(word)

    @raise_err.error
    async def raise_handler(self, ctx, err):
        if isinstance(err, BadInput):
            await ctx.send(f'{err}')
        else:
            await ctx.send(err)


    @commands.command(name='stream', aliases=['dumpit'])
    async def dump_it_all(self, ctx):
        col = Firebase.collection(ctx.guild.id)
        for doc in col.stream():
            data = doc.to_dict()
            print(f'Document data: {data}')
            await ctx.send('```' + f'{doc.id}\n' + str(data) + '```')

    @commands.command(name='rolecall')
    async def show_role_str(self, ctx, role:discord.Role):
        if role.mentionable:
            s = role.mention
            print(s)
            await ctx.send(s)
        else:
            await ctx.send('role not mentionable')



    @commands.command(name='matches')
    async def dump_all_matches(self, ctx, league:str):
        matches = Firebase.matches(ctx.guild.id, league)
        for round_ in matches.stream():
            round_data = round_.to_dict()
            dump = f'{round_.id} => {round_data}'
            print(dump)
            await ctx.send(dump)

    @commands.command(name='async')
    async def async_func(self, ctx, p:str=None):
        if p is not None:
            await Firebase.aio_matches(ctx.guild.id, 'something')
        else:
            await ctx.send('no param give')

    @commands.command(name='convert')
    async def convert_param(self, ctx, p:int):
        await ctx.send(f'type given is {type(p)}')
    
    @convert_param.error
    async def conversion_handler(self, ctx, error):
        '''
        This is apparantly not catchable from this context as the error type
        mutates before we get it and thus can not specify when a conversion 
        error happens
        '''
        if isinstance(error, ConversionError):
            await ctx.send(f'Could not convert {error.name} to proper form')
        else:
            await ctx.send(str(type(error)))


    @commands.command(name='reload')
    async def reload_cog(self, ctx, cog:str=None):
        if cog is None:
            await ctx.send(f'No cog specified')
        else:
            try:
                self.bot.unload_extension(cog)
                self.bot.load_extension(cog)
            except Exception as e:
                await ctx.send(f'**ERROR**: {type(e).__name__} {{ {e} }}')
            else:
                await ctx.send(f'{cog} reloaded')

def setup(bot):
    bot.add_cog(Testing(bot))
