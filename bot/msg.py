from discord import Embed
import etypes

async def new_embed(context, title=None, t='rich', color=0xFF0000, **fields):
    e = Embed(type=t, color=color, title=title)
    for k in fields:
        e.add_field(name=k, value=fields[k], inline=False)

    await context.send(embed=e)


async def err_embed(context, **fields):
    e = Embed(type='rich', color=etypes.err_color, title='Error')
    for k in fields:
        e.add_fields(name=k, value=fields[k], inline=False)

    await context.send(embed=e)

