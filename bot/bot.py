from datetime import datetime
from discord import Client
from os import getenv
from dotenv import load_dotenv
from discord.ext import commands
import firebase_admin
from firebase_admin import credentials, firestore
import discord, fireconn
from fireconn import Firebase

load_dotenv()

bot = commands.Bot(
    command_prefix=getenv('PREFIX'),
    description='shockrah is my creator :^) \nContact: shockrah#2647')

fireconn.Firebase.setup()

@bot.event
async def on_ready():
    print(f'logged in as {bot.user.name}')


# TODO: get a ban/kick event listener going
@bot.event
async def on_guild_join(guild):
    # add the guild as a new collection/hash to contain its events in
    if guild is None:
        return

    else:
        # firebase requires us to populate our document with _something_ so we fill it with bs
        meta_doc = Firebase.doc(guild.id, guild.id)
        meta_doc.set({'tmp-key': None})


@bot.event
async def on_member_ban(guild, member):
    col = Firebase.collection(guild.id)

    async def notify_team_ban(uids):
        names = ''.join([f'{guild.get_member(pid).name} ' for pid in uids])

        for pid in uids:
            user = guild.get_member(pid)
            try:
                d = await user.create_dm()
                await d.send('A member of your team was banned make sure to notify an admin when you find a replacement\n'+
                             'At this time these are the remaining players: ' + names)
            # just in case someone has us blocked
            except Exception:
                pass


    for event in col.stream():
        data = event.to_dict()
        # Avoid doing anything if they're not in the player registry
        try: data['players'].remove(member.id)
        except ValueError: continue

        if 'teams' not in data: continue

        for team in data['teams']:
            try: 
                print(f'Removing {member.id} and notifiying {team["members"]}')
                team['members'].remove(member.id)
                await notify_team_ban(team['members'])
                ref = Firebase.doc(guild.id, event.id)
                ref.update(data)
            except ValueError: 
                continue


def load_dev_env(bot):
    if getenv('DEV_COGS') is None:
        return
    else:
        for cog in getenv('DEV_COGS').split():
            print(f'Loading DEV extension: {cog}')
            bot.load_extension(cog)

if __name__ == '__main__':
    for ext in getenv('COG_LIST').split():
        print(f'Loading extension: {ext}')
        bot.load_extension(ext)

    load_dev_env(bot)

    bot.run(getenv('API_KEY'), bot=True, reconnect=True)

