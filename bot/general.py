from datetime import datetime

import discord
from discord.ext import commands
from discord.ext.commands import BadArgument, MissingRequiredArgument
from discord.ext.commands import UserInputError, MissingPermissions

import etypes, msg, matches
from fireconn import Firebase

class TimeError(Exception):
    pass

class General(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command(name='events')
    @commands.guild_only()
    async def list_events(self, ctx):
        '''
        .events => Active leagues listed on this server
        '''
        docs = Firebase.collection(ctx.guild.id).stream()
        events = {}
        for doc in docs:
            fields = doc.to_dict()
            desc = fields['description']
            events[str(doc.id)] = 'No description' if desc == '' else desc

        await msg.new_embed(ctx, **events)


    @list_events.error
    async def info_event_handler(self, ctx, err):
        if isinstance(err, MissingRequiredArgument):
            await msg.new_embed(ctx, **{
                'Format': '.format "Name of Tournament"'
            })
        else:
            print(err)
    


def setup(bot):
    bot.add_cog(General(bot))
