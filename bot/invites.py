import random, discord
from discord.ext import commands
from discord.ext.commands import BadArgument, MissingRequiredArgument
from discord.ext.commands import UserInputError, MissingPermissions
from inspect import Parameter


from fireconn import Firebase, LeagueData
from msg import new_embed
from helpers import dm_user

class Invites(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='invite')
    @commands.guild_only()
    async def create_invite(self, ctx, event:str):
        '''
        .invite "League name" Create a one time use join code
        '''
        event_ref = Firebase.doc(ctx.guild.id, event).get()
        if not event_ref.exists:
            raise UserInputError('League not found')

        teams_col = Firebase.teams(ctx.guild.id, event)
        event_meta = LeagueData.event_meta(ctx.guild.id, event)
        code = random.randint(0, 0xFFFFFFFF)
        for team in teams_col.stream():
            team_dict = team.to_dict()
            # This has to be done because firebase prevents us from 
            # keeping the collection empty so a meta data doc is required
            if 'members' not in team_dict: continue # this is also an awful bottleneck

            members = team_dict['members']
            if ctx.author.id in members and len(members) != event_meta['teamsize']:
                if not await dm_user(ctx.author, f'One time use code: \n```{code}```'):
                    await ctx.send(f':x: Unblock the bot')
                    return
                    
                # Code is usable
                if 'codes' in team_dict: 
                    team_dict['codes'].append(code)
                else:
                    team_dict['codes'] = [code]

                team.reference.update(team_dict)
                await ctx.send(':white_check_mark: Check your DM\'s for an invite code\n')
            else:
                await ctx.send('Can not invite anymore players')
                return

    def __team_limited(self, member, team, _max):
        return len(team) < _max and member not in team 


    def _joinable_member(self, code, team, meta, member):
        return (code in team['codes'] and
            len(team['members']) < meta['teamsize'] and
            member not in team['members'])

    @commands.command(name='join')
    @commands.guild_only()
    async def use_invite(self, ctx, code:str):
        '''
        .join <code> Join using a one time use join code

        Invite Codes are one time use and thus are removed once they are used
        '''
        # at some point this needs to be asynced
        events_col = Firebase.collection(ctx.guild.id)
        for event in events_col.stream():
            meta = event.to_dict()
            # ignore *pin documents
            if 'active' not in meta: continue

            for team in event.reference.collection('teams').stream():
                team_dict = team.to_dict()

                # Firebase requires a dummy doc to prevent collection deletion
                # so this check is necessary smh
                if 'members' not in team_dict: continue

                if self._joinable_member(code, team_dict, meta, ctx.author.id):

                        team_dict['codes'].remove(code)
                        team_dict['members'].append(ctx.author.id)

                        team.reference.update(team_dict)
                        await ctx.send(':white_check_mark:')



    @commands.command(name='signup')
    @commands.guild_only()
    async def join_tourney(self, ctx, league:str, teamname:str=None):
        '''
        .signup "TourneyName" "teamname"
        '''
        league_meta_ref = Firebase.doc(ctx.guild.id, league).get()
        if not league_meta_ref.exists: 
            await ctx.send('Event not found')
            return

        event_meta = league_meta_ref.to_dict()
        if event_meta['active'] is True:
            await ctx.send('League already active')
            return

        if teamname is None:
            if event_meta['teamsize'] > 1:
                raise MissingRequiredArgument(Parameter('teamname', Parameter.VAR_POSITIONAL))
            else:
                teamname = f'{ctx.author.id}'


        # Team mode registration
        team_ref = Firebase.teams(ctx.guild.id, league).document(teamname)
        team_ref_data = team_ref.get()
        if team_ref_data.exists:
            await ctx.send(f'Team with that name already exists `{{ {teamname} }}`')

        elif ctx.author.id in event_meta['players']:
            await ctx.send(':x: Already signed up as a player')

        else:
            # Create a new team
            team_ref.set({
                'codes': [],
                'members': [ctx.author.id],
                'name': teamname,
                'upcoming': None,
                'matches': []
            })
            # Update the player list and add the new team
            event_meta['players'].append(ctx.author.id)
            league_meta_ref.reference.update(event_meta)

            await new_embed(ctx, title=':white_check_mark:',**{
                'Inviting teammates': 'Create one time codes for each teammate with the command below',
                'Command':f'.invite "{league}"'
            })


    
    @join_tourney.error
    @use_invite.error
    @create_invite.error
    async def invite_err_handler(self, ctx, err):
        if isinstance(err, MissingRequiredArgument):
            await ctx.send(f'Missing argument {{ {err.param.name} }}')

        else:
            print(err)



def setup(bot):
    bot.add_cog(Invites(bot))
