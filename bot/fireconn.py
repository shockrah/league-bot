# Helper functions to setup connections to firebase
import asyncio as io

import firebase_admin
from firebase_admin import credentials, firestore

from google.cloud.firestore_v1.collection import CollectionReference

def get_client():
    cred = credentials.Certificate("creds.json")
    firebase_admin.initialize_app(cred)
    return firestore.client()

class Firebase(object):
    App = None

    @classmethod
    def setup(cls):
        if cls.App is None:
            _credentials = credentials.Certificate('creds.json')
            firebase_admin.initialize_app(_credentials)
            cls.App = firestore.client()
        else:
            print('The unthinkably stupid has finally happened')
            print('We actually have a case where we could not setup a firestore connection')


    @staticmethod
    def update(guild_id:int, event_id:int, data:dict):
        conn = Firebase.App
        ref = conn.collection(guild_id).document(event_id)
        ref.get().update(data)

    # conn/get_conn are meant to be aliases of the same thing
    @classmethod
    def conn(cls):
        return cls.get_conn()

    @classmethod
    def get_conn(cls):
        return cls.App

    @classmethod
    def doc(cls, guild_id:int, doc_id:str):
        return cls.App.collection(str(guild_id)).document(doc_id)


    @classmethod
    def event(cls, guild_id:int, doc_id:str):
        '''
        Alias for cls.doc(...)
        '''
        return cls.App.collection(str(guild_id)).document(doc_id)

    @classmethod
    def collection(cls, guild_id:int):
        '''
        Returns a reference to a top level collection within a server's list of collections
        '''
        return cls.App.collection(str(guild_id))

    @classmethod
    def matches(cls, guild_id:int, event_id:str):
        '''
        Returns a collection reference to the matches collection
        '''
        return cls.App.collection(str(guild_id)).document(event_id).collection('matches')

    @classmethod
    async def aio_matches(cls, guild_id:str, event_id:str):
        try:
            loop = io.get_running_loop()
            print('Running aio loop status: OK')
        except RuntimeError:
            print('No running loop from aio_matches')


    @classmethod
    def teams(cls, guild_id:int, event_id:str):
        '''
        Returns a collection reference to the teams collection
        '''
        return cls.App.collection(str(guild_id)).document(event_id).collection('teams')
    
    @classmethod
    def team(cls, guild_id:int, event_id:str, member_id:int):
        '''
        Gets the team of a player within a particular event
        '''
        for team in cls.teams(guild_id, event_id).stream():
            t_data = team.to_dict()
            if 'members' not in t_data: continue

            if member_id in t_data['members']:
                return team
        else:
            return None


class LeagueData(object):
    @staticmethod
    def event_meta(guild_id:int, event_id:str):
        '''
        Returns dictionary about the event's metadata
        '''
        doc = Firebase.doc(guild_id, event_id)
        data = doc.get().to_dict()
        return data



