from discord import Role
from discord.ext import commands
from discord.ext.commands import BadArgument, MissingRequiredArgument, UserInputError
import etypes, msg

from fireconn import Firebase
from helpers import dm_user

class Admins(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command(name='start')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def start_event(self, ctx, *, league:str=None):
        '''
        .start League Name...
        Change some meta settings
        Create a matches collection
        Setup matches between teams in the league
        '''
        if league is None:
            await msg.new_embed(ctx, **{
                'Format': '.start Name of league'
            })
            return

        event_ref = Firebase.doc(ctx.guild.id, league)
        meta_dict = event_ref.get().to_dict()
        meta_dict['active'] = True
        event_ref.update(meta_dict)

        # Collect a list of team-ids and pair them randomly together
        teams = Firebase.teams(ctx.guild.id, league)
        await ctx.send(f'{league} has now begun')
        

    @commands.command(name='create')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def create_event(self, ctx, 
        league_name:str, 
        role:Role,
        start_date:str, 
        rounds:int,
        team_size:int,
        max_score:int,
        *, description:str='No Description'):
        '''
        Format: .create "league name" @Role "start date" rounds(number) teamsize(number) max_score(number) desc.
        team_size => number
        '''
        if league_name == 'SERVER_META': 
            await ctx.send(f'Can not use {{ {league_name} }} as a league name')
            return

        if not role.mentionable:
            await ctx.send('Role is not mentionable!')
            return 

        if rounds < 1:
            await ctx.send('Invalid number of rounds for this league n >= 1')
            return 

        if team_size < 1: 
            await ctx.send('Invalid team size: must be greater than 1')
            return 

        if max_score < 1: 
            await ctx.send('Invalide max score: must be at least 1')
            return 

        # now we can put things into our db
        league_data = {
            'name': league_name,
            'startdate': start_date,
            'teamsize': team_size,
            'maxscore': max_score,
            'description': description,
            'role': role.id,
            'active': False,
            'players': [],
            'max-rounds': rounds,
            'round': 0,
        }

        event_doc= Firebase.doc(ctx.guild.id, league_name)
        if event_doc.get().exists:
            await ctx.send(f'League with that name already exists')

        else:
            event_doc.set(league_data)
            teams_col = Firebase.teams(ctx.guild.id, league_name).document('TEAMS_META')
            teams_col.set({})
            await msg.new_embed(ctx, **{
                'Name': league_name,
                'Role': role.mention,
                'Start date': start_date,
                'Description': description,
                'Rounds': rounds,
                'Teamsize': team_size,
                'Max score': max_score,
            })

            if await dm_user(ctx.author, '`.adminhelp` for detailed descriptions on managing the new league!'):
                print('good dm ')
            else:
                print('failed')

    @commands.command(name='delete')
    @commands.guild_only()
    @commands.has_permissions(administrator=True)
    async def delete_event(self, ctx, *, name:str):
        '''
        Deletes league completely!
        '''
        # name of the event is also the name of the document in the store
        event_doc = Firebase.doc(ctx.guild.id, name)
        if event_doc.get().exists:
            event_doc.delete()
            await ctx.send(f'{name}, has been deleted')
        else:
            await ctx.send(f'{name} does not exist')


    @create_event.error
    async def create_error_handler(self, ctx, e):
        if isinstance(e, BadArgument):
            await msg.new_embed(ctx, **{
                'Err':'One or more of the arguments were not understood/missing',
                'Note': 'Make sure event names with spaces have `"`quotes around them`"`'
            })

        elif isinstance(e, MissingRequiredArgument):
            await msg.new_embed(ctx, **{
                'Format':'.create `"league name"` @LeagueRole `Start date` `Rounds(number)` `Team size` `Max score` `Description...`',
                'Example': '.create "Weekend League #1" "January 3" 8 3 2 \nThe first season of the Weekend Warrior League!\nMatches are best of 3',
                'League name': 'Requires double quotes if the name has spaces in it',
                '@Role':'Role to assign players to: Must already exist',
                'Start date': 'Has no format but requires double quotes if it has spaces',
                'Rounds' : 'Number of matches that will be played, usually 1 per week [i.e. 16 rounds => 16 weeks]',
                'Team size + Max score':'Both must be numbers',
                'Description': 'Everything after the max score parameters',
            })

        elif isinstance(e, UserInputError):
            await ctx.send(f'Can not create a league with that name')
        else:
            print(e)


    @delete_event.error
    async def delete_err_handler(self, ctx, err):
        if isinstance(err, MissingRequiredArgument):
            await msg.new_embed(ctx, **{
                'Format': '.delete Name of League',
            })
        else:
            print(err)



def setup(bot):
    bot.add_cog(Admins(bot))
