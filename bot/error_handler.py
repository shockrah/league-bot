import traceback, sys
from discord.ext.commands import MissingPermissions
from discord.ext import commands
import discord

class CommandsErrorHandler(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        '''
        This handler fires after the local handlers so we only handle the most 
        commonly patterned exceptions here
        '''
        if isinstance(error, MissingPermissions):
            await ctx.send(f'Insufficient permissions to use that command')
        else:
            print('BEGIN ERROR', file=sys.stderr)
            print(ctx, error, file=sys.stderr)
            print('END ERROR', file=sys.stderr)


def setup(bot):
    bot.add_cog(CommandsErrorHandler(bot))
