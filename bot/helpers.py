async def dm_user(user, msg) -> bool:
    '''
    Params:
        @user -> discord.User obj to dm
        @msg -> str to send

    Returns -> bool
        @True on success
        @False on failure
    '''
    if user.dm_channel is None:
        await user.create_dm()

    # BUG: discordpy doesn't like dm'ing people for no particular reason 
    # this str8 up doesn't work i have no clue why
    try:
        await ctx.author.dm_channel.send(msg)
        return True
    except: 
        return False


def role_mention(id:int) -> str:
    return f'<@&{id}>'

