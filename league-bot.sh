#!/bin/bash

if [ ! -d 'bin/' ];then
	virtualenv .
	source bin/activate
	pip3 install -r requirements.txt
	python bot/bot.py
else
	source bin/activate
	python bot/bot.py
fi


