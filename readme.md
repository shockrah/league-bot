# League-Bot - Under Construction

A Discord bot built to support small communities setup tournaments and long running leagues easily and completely through Discord.

_All the code here is and always will be free and open source_ :^)

## Tech Stack

* Firebase for the multi server event tracking


# TODO

bot::rounds::generate_round

* Generate a new round of matches
* will likely need some helpers to sort out data properly

bot::admins::ffw

* Admins have final say on who gets a ffw when conflicts arise in scheduling

bot::admins::ffl

* Same thing as before but this time we dedicate a ffl to a team

bot::admins::role

* Assign a role to participants in a league
